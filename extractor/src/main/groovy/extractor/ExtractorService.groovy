package extractor

import groovy.xml.XmlParser
import io.micronaut.scheduling.annotation.Scheduled
import jakarta.inject.Inject
import jakarta.inject.Singleton

import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatterBuilder
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Singleton
class ExtractorService {

    @Inject
    TraficoProducer traficoProducer;

    @Scheduled(fixedDelay = "5m", initialDelay = "5s")
    void executeEveryFiveMinutes() {
        execute();
    }

    void execute(){
        String url = "https://informo.madrid.es/informo/tmadrid/pm.xml";
        try {
            def formatter = new DateTimeFormatterBuilder()
                    .appendPattern("dd/MM/yyyy H:mm:ss")
                    .toFormatter()
                    .withZone(ZoneId.of("Europe/Madrid"))
            def xml = new XmlParser().parse(url)
            
            def fechaHora = xml.fecha_hora.text()
            def parsed = LocalDateTime.parse(fechaHora,formatter)
            def tm = parsed.format(DateTimeFormatter.ISO_DATE_TIME)+".000Z"
            def list = []
            println "starting"
            int count=0
            xml.pm.each{ item ->
                if( !item.descripcion )
                    return
                def add = new EstadoUrbano(
                         idelem: item.idelem.text(),
                         fechahora: tm,
                         descripcion: item.descripcion.text(),
                         accesoAsociado: (item.accesoAsociado.text() ?: "0" )as int,
                         intensidad: (item.intensidad.text() ?: "0" )as int,
                         ocupacion: (item.ocupacion.text()  ?: "0" )as int,
                         carga: (item.carga.text()  ?: "0" )as int,
                         nivelServicio: (item.nivelServicio.text() ?: "0" )as int,
                         intensidadSat: (item.intensidadSat.text() ?: "0" )as int,
                         error: item.error.text(),
                         subarea: item.subarea.text() as int,
                         st_x: item.st_x.text(),
                         st_y: item.st_y.text(),
                )
                list.add add
                if( list.size() > 100 ){
                    traficoProducer.sendStatusUrbano(list)
                    count+=list.size()
                    list.clear()
                }
            }
            traficoProducer.sendStatusUrbano(list)
            count+=list.size()
            println "Enviado $count"
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
