package extractor

import io.micronaut.configuration.kafka.annotation.KafkaClient
import io.micronaut.configuration.kafka.annotation.Topic

@KafkaClient(id="trafico", batch = true)
interface TraficoProducer {

    @Topic("urbano")
    void sendStatusUrbano(List<EstadoUrbano> list)

}