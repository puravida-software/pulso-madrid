package analizer

import io.micronaut.core.annotation.Nullable
import io.micronaut.data.annotation.Id
import io.micronaut.data.annotation.MappedEntity

import javax.annotation.processing.Generated
import java.time.Instant

@MappedEntity("urbano")
class EstadoUrbanoEntity {

    @Id
    @Generated
    String idelem

    Instant fechahora1
    Integer nivelServicio1

    @Nullable
    Instant fechahora2
    @Nullable
    Integer nivelServicio2

    @Nullable
    Instant fechahora3
    @Nullable
    Integer nivelServicio3

    @Nullable
    Instant fechahora4
    @Nullable
    Integer nivelServicio4

    void push(Instant fechahora, Integer nivelServicio){
        fechahora4 = fechahora3
        nivelServicio4 = nivelServicio3

        fechahora3 = fechahora2
        nivelServicio3 = nivelServicio2

        fechahora2 = fechahora1
        nivelServicio2 = nivelServicio1

        fechahora1 = fechahora
        nivelServicio1 = nivelServicio
    }

}
