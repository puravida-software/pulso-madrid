package analizer

import io.micronaut.configuration.kafka.annotation.KafkaListener
import io.micronaut.configuration.kafka.annotation.OffsetReset
import io.micronaut.configuration.kafka.annotation.Topic
import io.micronaut.messaging.annotation.MessageBody
import jakarta.inject.Inject

import java.time.Instant

@KafkaListener(offsetReset = OffsetReset.EARLIEST)
class EstadoUrbanoConsumer {

    @Inject
    EstadoUrbanoRepository repository

    @Inject
    AlarmaService alarmaService

    @Topic("urbano")
    void receiveUrbano( @MessageBody EstadoUrbano bean){

        repository.findById(bean.idelem).ifPresentOrElse({EstadoUrbanoEntity entity ->
            entity.push(bean.fechahora, bean.nivelServicio)
            repository.update(entity)
            if( critico(entity) ){
                Alarma alarma = new Alarma(idelem: entity.idelem, fechaHora: entity.fechahora1)
                println "$entity en estado critico"
                alarmaService.notify(alarma)
            }
        },{
            EstadoUrbanoEntity entity = new EstadoUrbanoEntity(
                    idelem: bean.idelem,
                    fechahora1: bean.fechahora,
                    nivelServicio1: bean.nivelServicio
            )
            repository.save(entity)
        })

    }

    boolean critico( EstadoUrbanoEntity entity){
        entity.nivelServicio1 > 2 && entity.nivelServicio2 > 2 && entity.nivelServicio3 > 2
    }

}
