package analizer

import io.micronaut.configuration.kafka.annotation.KafkaClient
import io.micronaut.configuration.kafka.annotation.Topic

@KafkaClient(id="alarmas")
interface AlarmaService {

    @Topic("alarmas")
    void notify(Alarma alarma)

}