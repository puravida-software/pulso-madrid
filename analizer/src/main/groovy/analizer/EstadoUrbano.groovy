package analizer

import java.time.Instant


class EstadoUrbano {

    String idelem
    Instant fechahora
    String descripcion
    Integer accesoAsociado
    Integer intensidad
    Integer ocupacion
    Integer carga
    Integer nivelServicio
    Integer intensidadSat
    String error
    Integer velocidad
    Integer subarea
    String st_x
    String st_y

}
