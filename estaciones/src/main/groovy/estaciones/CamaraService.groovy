package estaciones

import groovy.xml.XmlSlurper
import io.micronaut.context.annotation.Context
import io.micronaut.core.io.ResourceLoader

import javax.annotation.PostConstruct

@Context
class CamaraService {

    List<Camara> camaras = []
    ResourceLoader resourceLoader

    CamaraService(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader
    }

    @PostConstruct
    void init(){
        String url = resourceLoader.getResourceAsStream("CCTV.kml").get().text
        def xml = url.substring(url.indexOf('<'))
        def kml = new XmlSlurper().parseText(xml).declareNamespace("xmlns":"http://earth.google.com/kml/2.2")
        kml.Document.Placemark.eachWithIndex{ item, idx->
            String description = item.description.text()
            String src = description.split(' ').find{ it.startsWith('src=')}.substring(4)
            def coord = item.Point.coordinates.text().split(',')
            Camara c = new Camara(
                    id: idx,
                    nombre:item.ExtendedData.Data[1].Value.text(),
                    url:src,
                    longitud: coord[0] as float,
                    latitud: coord[1] as float
            )
            camaras.add c
        }
    }

    List<Camara> closeTo(float latitude, float longitude, int size=3){
        def list = camaras.findAll()
        list.sort{ camara ->
            metersTo( camara.latitud, camara.longitud, latitude, longitude)
        }
        list.take(size)
    }

    static float metersTo(float lat1, float lng1, float lat2, float lng2) {
        double radioTierra = 6371;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double sindLat = Math.sin(dLat / 2);
        double sindLng = Math.sin(dLng / 2);
        double va1 = Math.pow(sindLat, 2) + Math.pow(sindLng, 2) * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));
        double va2 = 2 * Math.atan2(Math.sqrt(va1), Math.sqrt(1 - va1));
        double meters = radioTierra * va2;
        meters as float;
    }


}
