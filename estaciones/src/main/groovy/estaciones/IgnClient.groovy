package estaciones

import io.micronaut.core.annotation.Nullable
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Header
import io.micronaut.http.annotation.Post
import io.micronaut.http.client.annotation.Client

@Client("http://www.ign.es/wps-transformacion")
interface IgnClient {

    @Post(uri = "/servicios", consumes =  MediaType.TEXT_PLAIN)
    String convert(@Body String xml, @Nullable @Header(name = "Content-type", value = "application/xml") String contentType)

}