package estaciones

import io.micronaut.configuration.kafka.annotation.KafkaListener
import io.micronaut.configuration.kafka.annotation.OffsetReset
import io.micronaut.configuration.kafka.annotation.Topic

@KafkaListener(offsetReset = OffsetReset.EARLIEST)
class AlarmaService {

    EstacionRepository estacionRepository

    AlarmaService(EstacionRepository estacionRepository) {
        this.estacionRepository = estacionRepository
    }

    @Topic("alarmas")
    void alarma(Alarma alarma){
        estacionRepository.findById(alarma.idelem).ifPresent({entity ->
            entity.ultimaAlarma = alarma.fechaHora
            estacionRepository.update(entity)
        })
    }

}
