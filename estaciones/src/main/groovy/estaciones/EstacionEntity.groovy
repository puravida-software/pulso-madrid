package estaciones

import io.micronaut.core.annotation.Nullable
import io.micronaut.data.annotation.Id
import io.micronaut.data.annotation.MappedEntity
import jakarta.persistence.Transient

import javax.annotation.processing.Generated
import java.time.Instant

@MappedEntity("estaciones")
class EstacionEntity {

    @Id
    @Generated
    String idelem

    String descripcion
    Float latitud
    Float longitud

    @Nullable
    Instant ultimaAlarma

    @Transient
    String getMapURL(){
        "https://www.openstreetmap.org/#map=19/${latitud}/${longitud}"
    }

}
