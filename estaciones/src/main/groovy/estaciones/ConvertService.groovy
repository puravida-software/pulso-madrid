package estaciones

import groovy.xml.XmlSlurper
import io.micronaut.context.annotation.Context
import jakarta.inject.Singleton

import javax.annotation.PostConstruct

@Context
@Singleton
class ConvertService {

    IgnClient ignClient

    ConvertService(IgnClient ignClient) {
        this.ignClient = ignClient
    }

    @PostConstruct
    void init(){
        def x = '436039.395885266'
        def y = '4472397.54735486'
        println locate(x,y)
    }


    double[] locate(String x, String y){
        def request = """<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<wps:Execute service="WPS" version="1.0.0" xmlns:wps="http://www.opengis.net/wps/1.0.0" xmlns:ows="http://www.opengis.net/ows/1.1" xmlns:xlink="http://www.w3.org/1999/xlink" 
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/wps/1.0.0 http://schemas.opengis.net/wps/1.0.0/wpsExecute_request.xsd">
<ows:Identifier>TransformCoordinates</ows:Identifier><wps:DataInputs><wps:Input><ows:Identifier>SourceCRS</ows:Identifier><wps:Data><wps:LiteralData>epsg:25830</wps:LiteralData></wps:Data>
</wps:Input><wps:Input><ows:Identifier>TargetCRS</ows:Identifier><wps:Data><wps:LiteralData>epsg:4230</wps:LiteralData></wps:Data></wps:Input><wps:Input><ows:Identifier>InputData</ows:Identifier>
<wps:Data><wps:ComplexData mimeType="text/xml; subtype=gml/3.1.1"><gml:FeatureCollection xmlns:gml="http://www.opengis.net/gml" xmlns:wfs="http://www.opengis.net/wfs" xmlns:p="http://example.org">
<gml:featureMember><p:Point><gml:pointProperty><gml:Point srsName="epsg:25830">

<gml:pos srsDimension="2">${x.replaceAll(",",".")} ${y.replaceAll(",",".")}</gml:pos>

</gml:Point></gml:pointProperty></p:Point></gml:featureMember></gml:FeatureCollection></wps:ComplexData></wps:Data></wps:Input></wps:DataInputs><wps:ResponseForm>
<wps:RawDataOutput mimeType="text/xml; subtype=gml/3.1.1">
<ows:Identifier>TransformedData</ows:Identifier></wps:RawDataOutput>
</wps:ResponseForm></wps:Execute>"""

        def response = ignClient.convert(request,null)
        def xml = new XmlSlurper().parseText(response)
        "$xml".split(" ").collect{ it as double}
    }

}
