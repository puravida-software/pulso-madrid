package estaciones

import io.micronaut.data.jdbc.annotation.JdbcRepository
import io.micronaut.data.model.query.builder.sql.Dialect
import io.micronaut.data.repository.CrudRepository

import java.time.Instant

@JdbcRepository(dialect = Dialect.POSTGRES)
interface EstacionRepository extends CrudRepository<EstacionEntity, String>{

    Iterable<EstacionEntity> findByUltimaAlarmaGreaterThan(Instant when)

}
