package estaciones

import io.micronaut.configuration.kafka.annotation.KafkaListener
import io.micronaut.configuration.kafka.annotation.OffsetReset
import io.micronaut.configuration.kafka.annotation.Topic
import io.micronaut.messaging.annotation.MessageBody

@KafkaListener(offsetReset = OffsetReset.EARLIEST)
class EstacionListener {

    EstacionRepository estacionRepository
    ConvertService convertService

    EstacionListener(EstacionRepository estacionRepository, ConvertService convertService) {
        this.estacionRepository = estacionRepository
        this.convertService = convertService
    }

    @Topic("urbano")
    void receiveUrbano( @MessageBody EstadoUrbano bean){
        if( !estacionRepository.findById(bean.idelem).present ){
            double[] coordenadas = convertService.locate(bean.st_x, bean.st_y)
            EstacionEntity entity = new EstacionEntity(
                    idelem: bean.idelem,
                    descripcion: bean.descripcion,
                    latitud: coordenadas[1],
                    longitud: coordenadas[0]
            )
            estacionRepository.save(entity)
        }
    }

}
