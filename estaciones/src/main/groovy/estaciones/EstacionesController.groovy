package estaciones

import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.server.types.files.StreamedFile

import java.time.Instant
import java.time.temporal.ChronoUnit

@Controller("/api")
class EstacionesController {

    EstacionRepository estacionRepository
    CamaraService camaraService

    EstacionesController(EstacionRepository estacionRepository, CamaraService camaraService) {
        this.estacionRepository = estacionRepository
        this.camaraService = camaraService
    }

    @Get
    Iterable<EstacionEntity>list(){
        estacionRepository.findAll()
    }

    @Get("/alarmas")
    Iterable<EstacionEntity>listAlarma(){
        def from = Instant.now().minus(10, ChronoUnit.MINUTES)
        estacionRepository.findByUltimaAlarmaGreaterThan(from)
    }


    @Get('/{id}')
    Optional<EstacionEntity> estacion(String id){
        estacionRepository.findById(id)
    }

    @Get('/{id}/camaras')
    Iterable<Camara> camaras(String id){
        EstacionEntity entity = estacionRepository.findById(id).orElseThrow()
        camaraService.closeTo(entity.latitud, entity.longitud)
    }

    @Get('/{id}/camaras/{pos}')
    StreamedFile camara(String id, int pos){
        assert pos < 4
        EstacionEntity entity = estacionRepository.findById(id).orElseThrow()
        def camara = camaraService.closeTo(entity.latitud, entity.longitud, 4)[pos-1]
        new StreamedFile(camara.url.toURL().openStream(), MediaType.IMAGE_JPEG_TYPE)
    }
}
